import networkx as nx
#import networkx.drawing
import matplotlib.pyplot as plt


#from networkx.drawing.tests.test_pylab import plt

print("\n-----------------NODES--------------------\n")

G = nx.Graph()
G.add_node(1) #add one node
print("G.add_node(1) -> G number of nodes : ", G.number_of_nodes())
G.add_nodes_from([2, 3]) #add a list of nodes
print("G.add_nodes_from([2, 3]) -> G number of nodes : ", G.number_of_nodes())

#add any iterable container of nodes
H = nx.path_graph(10)
print("H = nx.path_graph(10) -> H number of nodes : ", H.number_of_nodes())

I = nx.Graph()
I.add_nodes_from(H) #use nodes of graph H - graph of nodes
print("I.add_nodes_from(H) -> I number of nodes : ", I.number_of_nodes())

J = nx.Graph()
J.add_node(H) #aussi possible (direct use the graph)  - graph of graph
print("J.add_node(H) -> J number of nodes : ", J.number_of_nodes(), " here the node is a graph")

print("\n-----------------EDGES--------------------\n")

G.add_edge(1, 2)
print("G.add_edge(1, 2) -> G number of edges : ", G.number_of_edges())

e = (2, 3)
G.add_edge(*e)  # unpack edge tuple*
print("e = (2, 3), G.add_edge(*e) -> G number of edges : ", G.number_of_edges())

#Aussi possible
G.add_edges_from([(1, 4), (1, 3)])
print("G.add_edges_from([(1, 4), (1, 3)]) -> G number of edges : ", G.number_of_edges(), "Here a new node was insterted (4)")

G.add_edges_from(H.edges)
print("G.add_edges_from(H.edges) -> G number of edges : ", G.number_of_edges())

print("\n-----------------COMPLETE FIRST EXAMPLE--------------------\n")

G.clear()
print("G.clear() -> G number of nodes : ", G.number_of_nodes(), " |  G number of edges : ", G.number_of_edges())

G.add_edges_from([(1, 2), (1, 3)])
G.add_node(1)
G.add_edge(1, 2)
G.add_node("spam")        # adds node "spam"
G.add_nodes_from("spam")  # adds 4 nodes: 's', 'p', 'a', 'm'
G.add_edge(3, 'm')

print("\nG.add_edges_from([(1, 2), (1, 3)])\n"
      "G.add_node(1)\n"
      "G.add_edge(1, 2)\n"
      "G.add_node(\"spam\")\n"
      "G.add_nodes_from(\"spam\")\n"
      "G.add_edge(3, 'm') -> G number of nodes : ", G.number_of_nodes(), " |  G number of edges : ", G.number_of_edges())

print("\nlist(G.nodes) -> ",list(G.nodes),"\n"
      "list(G.edges) -> ",list(G.edges),"\n"
      "list(G.adj[1]) -> ",list(G.adj[1]),"\n" # or list(G.neighbors(1))
      "lG.degree[1] -> ", G.degree[1], "\n") # the number of edges incident to 1

print("\nG.edges([2, 'm']) -> ", G.edges([2, 'm']),"\n"
      "G.degree([2, 3]) -> ", G.edges([2, 'm']))

G.remove_node(2)
G.remove_nodes_from("spam")
print("\nG.remove_node(2)\n"
      "G.remove_nodes_from(\"spam\") -> list(G.nodes) : ", list(G.nodes), " |  list(G.edges) : ", list(G.edges))

G.remove_edge(1, 3)
print("\nG.remove_edge(1, 3) -> list(G.nodes) : ", list(G.nodes), " |  list(G.edges) : ", list(G.edges))

G.clear()
print("\nG.clear() -> list(G.nodes) : ", list(G.nodes), " |  list(G.edges) : ", list(G.edges))

G.add_edge(1, 2)
H = nx.DiGraph(G)   # create a DiGraph using the connections from G
print("\nG.add_edge(1, 2)\n"
      "H = nx.DiGraph(G) -> list(H.edges()) : ",list(H.edges()))

edgelist = [(0, 1), (1, 2), (2, 3)]
H = nx.Graph(edgelist)
print("\nedgelist = [(0, 1), (1, 2), (2, 3)]\n"
      "H = nx.Graph(edgelist) -> list(H.edges()) : ",list(H.edges()))

G[1] # same as G.adj[1]
G[1][2]
G.edges[1, 2]
print("\nG[1] : ", G[1],"\n"
      "G[1][2]", G[1][2],"\n"
      "G.edges[1, 2]",G.edges[1, 2]," -> list(G.nodes) : ", list(G.nodes), " |  list(G.edges) : ", list(G.edges))

G.add_edge(1, 3)
G[1][3]['color'] = "blue"
G.edges[1, 2]['color'] = "red"
print("\nG.add_edge(1, 3) : \n"
      "G[1][3]['color'] = \"blue\"\n"
      "G.edges[1, 2]['color'] = \"red\" -> list(G.nodes) : ", list(G.nodes), " |  list(G.edges) : ", list(G.edges))

#adjacency iteration sees each edge twice.
print("\nFG = nx.Graph()\n"
"FG.add_weighted_edges_from([(1, 2, 0.125), (1, 3, 0.75), (2, 4, 1.2), (3, 4, 0.375)])\n"
"for n, nbrs in FG.adj.items():\n"
"   for nbr, eattr in nbrs.items():\n"
"        wt = eattr['weight']\n"
"        if wt < 0.5: print('(%d, %d, %.3f)' % (n, nbr, wt))")
FG = nx.Graph()
FG.add_weighted_edges_from([(1, 2, 0.125), (1, 3, 0.75), (2, 4, 1.2), (3, 4, 0.375)])
for n, nbrs in FG.adj.items():
   for nbr, eattr in nbrs.items():
        wt = eattr['weight']
        if wt < 0.5: print('(%d, %d, %.3f)' % (n, nbr, wt))

print("\nfor (u, v, wt) in FG.edges.data('weight'):\n"
"      if wt < 0.5: print('(%d, %d, %.3f)' % (u, v, wt))")
for (u, v, wt) in FG.edges.data('weight'):
      if wt < 0.5: print('(%d, %d, %.3f)' % (u, v, wt))

print("\n-----------------DRAWING GRAPHS FIRST EXAMPLE--------------------\n")

G = nx.petersen_graph()
plt.subplot(121)
nx.draw(G, with_labels=True, font_weight='bold')
plt.subplot(122)
nx.draw_shell(G, nlist=[range(5, 10), range(5)], with_labels=True, font_weight='bold')

plt.show()
from Classes.Availability import Availability


class Collector:
    def __init__(self, id_collector, location, availability, storing_capacity):
        self.id_collector = int(id_collector)
        self.location = location

        availability = availability.replace("[", "")
        availability = availability.replace("]", "")
        availability = availability.replace("(", "")
        availability = availability.replace(")", "")
        availability = availability.replace(" ", "")
        availability = availability.split(',')

        availabilities = []
        start_time = 0
        for i in range(len(availability)):
            if i % 2 == 0:
                start_time = availability[i]
            else:
                end_time = availability[i]
                av = Availability(start_time, end_time)
                availabilities.append(av)

        self.availabilities = availabilities
        self.storing_capacity = int(storing_capacity)

class Carrier:
    def __init__(self, id_carrier,location_origin,location_destination,time_origin,time_destination,max_detour,carrying_capacity):
        self.id_carrier = int(id_carrier)
        self.location_origin = location_origin
        self.location_destination = location_destination
        self.time_origin = int(time_origin)
        self.time_destination = int(time_destination)
        self.max_detour = int(max_detour)
        self.carrying_capacity = int(carrying_capacity)
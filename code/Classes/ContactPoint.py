from enum import Enum


class PathType(Enum):
    DEPARTURE = 1
    ARRIVAL = 2


class ContactPoint:
    def __init__(self, pathType, car, col, time):
        self.pathType = pathType
        self.car = car
        self.col = col
        self.time = int(time)

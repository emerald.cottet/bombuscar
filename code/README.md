BombusCar
=========

Le but de ce projet et de réaliser la création d'un réseau de distribution intelligent, flexible et sécurisé pour le transport de colis en exploitant des capacités de transport déjà existantes.
Plus précisément, il est question de la création automatique du graph du réseau à partir des utilisateurs inscrits dans la plateforme et par la suite effectuer des tests avec des algorithmes d'optimisation.


Prérequis
---------

- Python 3.5
- OR-Tools
- NetworkX

Installation
------------

Pour installer ...

	...

Lancement
---------

Pour lancer ...

	...

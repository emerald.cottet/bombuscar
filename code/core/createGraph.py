import sys

import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import ticker

from Classes.ContactPoint import ContactPoint, PathType
from core.createInstancesFromCVS import InstancesCreator

# Instanciation des données
i = InstancesCreator()

contactPoints = []

# Instanciation des Contact Points (nodes)
for col in i.collectors:
    for car in i.carriers:
        if col.location == car.location_origin:
            cp = ContactPoint(PathType.DEPARTURE, car, col, car.time_origin)
            contactPoints.append(cp)
        if col.location == car.location_destination:
            cp = ContactPoint(PathType.ARRIVAL, car, col, car.time_destination)
            contactPoints.append(cp)

"""
----------------------------------------------------------------------------------------------------------------
    Create Graph - Method to create png file from graph
----------------------------------------------------------------------------------------------------------------
"""


def createPng(graph, fileName):
    # Config size of figure
    plt.figure(figsize=(30, 10))

    fig, ax = plt.subplots(figsize=(30, 10))
    plt.title('BombusCar Graph')

    # Positions for nodes
    pos = {}
    for currentNode in graph.nodes():
        pos[currentNode] = (int(currentNode.time), ord(currentNode.col.location))

    # Draw nodes
    nx.draw_networkx_nodes(graph, pos, node_size=300, ax=ax)

    # Draw edges
    nx.draw_networkx_edges(graph, pos, node_size=300, arrowsize=50, ax=ax)

    # Positions for labels
    posLabels = {}
    for currentNode in graph.nodes():
        posLabels[currentNode] = (int(currentNode.time)+20, ord(currentNode.col.location)+0.2)

    # Labels of nodes
    labels = {}
    for currentNode in graph.nodes():
        labels[currentNode] = str(currentNode.car.id_carrier) + "\n" + str(
            currentNode.col.id_collector) + "\n" + currentNode.col.location + "\n" + str(
            currentNode.time)
    nx.draw_networkx_labels(graph, posLabels, labels, font_size=16, font_family="sans-serif", ax=ax)

    # Labels of edges
    edgeLabels = {}
    for currentEdge in graph.edges():
        if 'weight' in graph[currentEdge[0]][currentEdge[1]]:
            edgeLabels[currentEdge] = graph[currentEdge[0]][currentEdge[1]]['weight']
    nx.draw_networkx_edge_labels(graph, pos, edgeLabels, font_size=16, font_family="sans-serif",
                                 ax=ax)

    # Axis
    labelsY = ax.get_yticks().tolist()
    for j in range(0, len(labelsY)):
        labelsY[j] = '' + chr(int(labelsY[j]))
    ax.set_yticklabels(labelsY)
    ax.set_ylim(64.8, 67.4)
    ax.xaxis.set_ticks(np.arange(0, 1300, 100))
    ax.yaxis.set_ticks(np.arange(65, 68, 1))
    ax.yaxis.set_major_formatter(ticker.FormatStrFormatter('%c'))
    ax.set_axis_on()
    plt.tick_params(left=True, bottom=True, labelleft=True, labelbottom=True)

    # Create png from plot
    plt.savefig('../output/' + fileName)

    # plt.show()


"""
----------------------------------------------------------------------------------------------------------------
    Create Graph - Step 1 : all contact Point considered
----------------------------------------------------------------------------------------------------------------
"""

# Create Graph
G = nx.DiGraph()

# Add nodes
G.add_nodes_from(contactPoints)

# Add edges
for cp in contactPoints:
    if cp.pathType == PathType.DEPARTURE:
        for cp2 in contactPoints:
            if cp2.pathType == PathType.ARRIVAL and cp.car.id_carrier == cp2.car.id_carrier:
                G.add_edge(cp, cp2)

createPng(G, 'graph_step1.png')

"""
----------------------------------------------------------------------------------------------------------------
    Create Graph - Step 2 : Pruning unavailability period.
        We remove carriers’ “edge” starting or arriving during collector unavailability
----------------------------------------------------------------------------------------------------------------
"""
# Create Graph
G2 = G

# Remove nodes and edges
nodesToRemove = []
for node in list(G2.nodes):
    isAvailable = False
    for av in node.col.availabilities:
        if av.start_time <= node.time <= av.end_time:
            isAvailable = True
    if not isAvailable:
        nodesToRemove.append(node)
for edge in list(G2.edges):
    for node in nodesToRemove:
        if edge[0] == node or edge[1] == node:
            G2.remove_node(edge[0])
            G2.remove_node(edge[1])

createPng(G2, 'graph_step2.png')
"""
----------------------------------------------------------------------------------------------------------------
    Create Graph - Step 3 : Create new edges to extend Collectors
----------------------------------------------------------------------------------------------------------------
"""
# Create Graph
G3 = G2

# Add new edges
for n in list(G3.nodes):
    minNextNode = None
    minTime = sys.maxsize
    for n2 in list(G3.nodes):
        if n.col.location == n2.col.location and n.time < n2.time < minTime:
            minTime = n2.time
            minNextNode = n2
    if minNextNode is not None:
        G3.add_edge(n, minNextNode)

createPng(G3, 'graph_step3.png')

"""
----------------------------------------------------------------------------------------------------------------
    Create Graph - Step 4 : Add weights on edges
----------------------------------------------------------------------------------------------------------------
"""
# Create Graph
G4 = G3

# Add weights on edges
for e in G4.edges():
    G4[e[0]][e[1]]['weight'] = e[1].time - e[0].time

createPng(G4, 'graph_step4.png')

# plt.show()

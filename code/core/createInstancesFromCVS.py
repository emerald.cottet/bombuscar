import csv

from Classes.Carrier import Carrier
from Classes.Collector import Collector
from Classes.Package import Package


# Class used to create instances of Carrier, Collector and Package from cvs files
class InstancesCreator:
    """
    ----------------------------------------------------------------------------------------------------------------
        Attributes
    ----------------------------------------------------------------------------------------------------------------
    """
    carriers = []  # List of instances of Carrier
    collectors = []  # List of instances of Collector
    packages = []  # List of instances of Package

    """
    ----------------------------------------------------------------------------------------------------------------
        Methods
    ----------------------------------------------------------------------------------------------------------------
    """

    # Constructor
    def __init__(self):
        self.createInstances()

    def createInstances(self):
        # Create instances of Carrier from file Carriers.cvs
        with open('../Data/Carriers.csv', newline='') as csvFile:
            reader = csv.reader(csvFile, delimiter=',')
            isFirstLine = True
            for row in reader:
                if isFirstLine:
                    isFirstLine = False
                    continue
                c = Carrier(row[0], row[1], row[2], row[3], row[4], row[5], row[6])
                self.carriers.append(c)

        # Create instances of Collector from file Collectors.cvs
        with open('../Data/Collectors.csv', newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            isFirstLine = True
            for row in reader:
                if isFirstLine:
                    isFirstLine = False
                    continue
                c = Collector(row[0], row[1], row[2], row[3])
                self.collectors.append(c)

        # Create instances of Package from file Packages.cvs
        with open('../Data/Packages.csv', newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            isFirstLine = True
            for row in reader:
                if isFirstLine:
                    isFirstLine = False
                    continue
                p = Package(row[0], row[1], row[2])
                self.packages.append(p)

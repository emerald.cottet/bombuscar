Journal de travail : Cottet Emerald
===================================

Semaine A17
-----------
 - 30.01: 60 / Gestion de projet (ajustement des documents sur gitlab)
 - 30.01: 420 / Rapport (fin de rédaction)
 - 30.01: 300 / Implémentation (fin et ajustements)
 - 29.01: 480 / Implémentation (finalisation)
 - 28.01: 600 / Implémentation (poursuite et debbuging)
 - 27.01: 480 / Implémentation (poursuite et debbuging)

Semaine A16
-----------
 - 25.01: 120 / Rapport et Implémentation (poursuite)
 - 24.01: 120 / Implémentation (poursuite générale)
 - 23.01: 30 / Rédaction du PV11
 - 22.01: 120 / Implémentation (début d'unification avec ce que QS à mis en place)
 - 22.01: 30 / Séance de groupe (organisation pour la fin du projet, implémenation - Quentin Seydoux, Francesco Carrino)

Semaine A15
-----------
 - 15.01: 120 / Implémentation (approche svg to png pour les graphes)

Semaine A14
-----------
 - 08.01: 150 / Implémentation (essais d'affichage de graphes avec OR-Tools)

Semaine A13
-----------
 - 03.01: 120 / Implémentation (poursuite des essais)
 - 18.12: 90 / Implémentation (essais avec exemples d'OR-Tools)
 - 18.12: 30 / Séance de groupe (organisation pour la suite du projet avec l'implémentation - Quentin Seydou, Francesco Carrino)

Semaine A12
-----------
 - 12.12: 15 / Rédaction du PV9
 - 11.12: 60 / Corrections sur le rapport
 - 11.12: 30 / Séance de groupe (discussion analyse+conception - Quentin Seydoux, Francesco Carrino, Omar Abou Khaled)
 - 09.12: 60 / Avancement rédaction du rapport

Semaine A11
-----------
 - 04.12: 100 / Avancement rédaction du rapport
 - 04.12: 60 / Recherches et réflexions (pour l'analyse et la conception) 

Semaine A10
-----------
 - 27.11: 60 / Avancement-corrections de l'analyse
 - 27.11: 15 / Discussion sur la conception avec Francesco Carrino
 - 27.11: 30 / Séance de groupe (discussion sur l'analyse - Quentin Seydoux, Francesco Carrino, Omar Abou Khaled)
 - 26.11: 60 / Rapport (partie analyse)

Semaine A9
----------
 - 20.11: 180 / Divers (avancement rapport d'analyse - recherches - or-tools) 

Semaine A8
----------
 - 13.11: 120 / Divers (avancement rapport d'analyse - essais OR-Tools - PV)
 - 13.11: 15 / Séance de groupe (discussion sur l'analyse - Quentin Seydoux, Francesco Carrino, Oma Abou Khaled)
 - 13.11: 60 / OR-Tools (essais avec librairies particulière)

Semaine A7
----------
 - 06.11: 120 / Rapport (avancement de la rédaction de l'analyse) 
 - 06.11: 90 / OR-Tools (passage à linux - découverte approfondie de l'outil avec des librairies)
 - 04.11: 90 / OR-Tools (gestion de l'environnement pour l'outil - régalges de nombreux problèmes)

Semaine A6
----------
 - 30.10: 100 / OR-Tools (prise en main de l'outil fourni par Google)
 - 30.10: 15 / Séance de groupe (retour sur la présentation, discussion pour la suite - Quentin Seydoux, Francesco Carrino via skype)
 - 30.10: 60 / Divers (poursuite du projet, préparation pour la séance de groupe)
 - 29.10: 30 / Gestion générale (gérer le git, préparer des éléments pour la suite, etc)

Semaine A5
----------
 - 16.10: 180 / Présentation intermédiaire (suivi des présentations des autres projets, présentation de BombusCar - Quentin Seydoux, Francesco Carrino, Elena Mugelini, Omar Abou Khaled)
 - 15.10: 60 / Présentation intermédiaire (préparation pour la présentation orale)
 - 15.10: 60 / Présentation intermédiaire (fin du powerpoint)
 - 14.10: 120 / Présentation intermédiaire (compléments de contenu pour le powerpoint + animations)

Semaine A4
----------
 - 08.10: 60 / Analyse des contraintes (premières recherches et réflexions pour les contraintes sous la forme d'un brainstorming)
 - 07.10: 120 / Présentation intermédiaire (ésquisse étoffée du powerpoint pour la présentation intermédiaire)

Semaine A3
----------
 - 05.10: 60 / Déploiement + essais code python (mise en place de l'infrastructure pour pouvoir lancer le code fourni par Francesco Carrino, premiers essais en essayant de comprendre ce qu'il se passe)
 - 02.10: 60 / Mise en place des logbooks (journal de travail avec choix format .md, voir README dans le dossier "Journal de travail" pour le détail formattage)
 - 02.10: 35 / Séance de groupe (discussion sur le cahier des charges+planning - Francesco Carrino, Quentin Seydoux, Omar Abou Khaled via skype)
 - 02.10: 60 / Fin rédaction cahier des charges + planning + relecture (version 1)
 - 01.10: 60 / Rédaction cahier des charges + planning (version 1)
 - 30.09: 40 / Préparation cahier des charges + planning (formats, début rédaction) 

Semaine A2
----------
 - 26.09: 80 / Rédaction pv's des 2 entretiens
 - 25.09: 50 / Seconde séance (discussion approfondie sur le concept du projet - Francesco Carrino, Quentin Seydoux)
 - 25.09: 30 / Première séance avec les superviseurs du projet (lancement officiel du projet - Omar Abou Khaled, Francesco Carrino, Quentin Seydoux)
 - 25.09: 70 / Préparation envirronement de travail (gitlab, formats rapport+pv)

Semaine A1
----------
 - 18.09: 240 / Découverte des projets de semestre 5 + choix du projet BombusCar (projet en binôme, Quentin Seydoux)

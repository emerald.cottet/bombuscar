Journal de travail : Seydoux Quentin
====================================

Semaine A17
----------
 - 30.01: 240 / Implémentation : Rédaction et finalisation du rapport.
 - 30.01: 180 / Implémentation : Ajout de nouveaux edges extended entre les Collectors, ajout de poids sur les edges et travail sur l'affichage des labels.
 - 29.01: 180 / Implémentation : Pruning, prise en compte des fenêtres de temps des collectors. Suppression des noeuds dans un deuxième graphe.
 - 29.01: 60 / Implémentation : changement des fichiers cvs pour travailler sur un graphe simple.
 - 28.01: 120 / Implémentation : création des edges dans le graphe
 
Semaine A16
----------
 - 26.01: 120 / Implémentation : création des nodes dans le graphe
 - 22.01: 120 / Implémentation : contact point et mise en place pour la création de graphe.
 - 22.01: 30 / Séance de groupe (organisation pour la fin du projet et implémentation - Emerald Cottet, Francesco Carrino)
 - 22.01: 15 / Préparation pour la séance de groupe
 - 21.01: 30 / Discussion avec M. Carrino : feedback implémentation et organisation pour la suite du projet.

Semaine A15
----------

Semaine A14
----------
 - 08.01: 30 / Implémentation : Préparation de listes d'objets et du fichier de création du graphe
 - 08.01: 120 / Implémentation : Création de classes pour Carrier, Collector et Package et instantiation d'objets à partir des fichiers csv
 - 08.01: 30 / Implémentation : Réflexion sur le stockage des données d'entrée
 - 08.01: 60 / Implémentation : Lecture des fichiers csv
 
Semaine A13
----------
 - 18.12: 60 / Implémentation : Dessin d'un graphe - tutoriel avec NetworkX
 - 18.12: 60 / Mise en place de l'environnement pour l'implémentation
 - 18.12: 30 / Corrections sur le rapport d'analyse et de conception
 - 18.12: 30 / Rédaction pv de la séance
 - 18.12: 30 / Séance de groupe (organisation pour la suite du projet et l'implémentation - Emerald Cottet, Francesco Carrino)
 - 18.12: 15 / Préparation pour la séance de groupe
 
Semaine A12
----------
 - 11.12: 30 / Séance de groupe (discussion sur le rapport d'analyse et le rapport de conception - Emerald Cottet, Francesco Carrino, Omar Abou Khaled)
 - 30.10: 15 / Préparation pour la séance de groupe
 - 10.12: 120 / Rédaction du rapport de conception (finalisation).
 
Semaine A11
----------
 - 04.12: 180 / Travail de conception (UML, entrées et sorties, architecture). Rédaction du rapport de conception. 
 - 04.12: 30 / Réorganisation du rapport (séparation annexes, template)

Semaine A10
----------
 - 27.11: 15 / Discussion sur la conception avec Francesco Carrino
 - 27.11: 30 / Rédaction pv de la séance
 - 27.11: 30 / Séance de groupe (discussion sur l'analyse - Emerald Cottet, Francesco Carrino, Omar Abou Khaled)
 - 26.11: 120 / Rédaction du rapport
 
Semaine A9
----------
 - 20.11: 30 / Réalisation de la page de titre pour le rapport
 - 20.11: 120 / Création du template pour le rapport du projet
 - 20.11: 120 / Rédaction des guides d'installations

Semaine A8
----------
 - 13.11: 120 / Divers (NetworkX, analyse)
 - 13.11: 15 / Séance de groupe (discussion sur l'analyse - Emerald Cottet, Francesco Carrino, Omar Abou Khaled)
 - 13.11: 60 / Divers (poursuite du projet, préparation pour la séance de groupe)
 
Semaine A7
----------
 - 06.11: 120 / Rédaction des guides d'installations
 - 06.11: 60 / NetworkX (passage sur linux)
 - 06.11: 120 / NetworkX (prise en main de l'outil)

Semaine A6
----------
 - 30.10: 120 / NetworkX (prise en main de l'outil)
 - 30.10: 30 / Rédaction pv de la séance
 - 30.10: 15 / Séance de groupe (discussion sur la présentation + analyse - Emerald Cottet, Francesco Carrino via skype)
 - 30.10: 60 / Divers (poursuite du projet, préparation pour la séance de groupe)

Semaine A5
----------
 - 16.10: 180 / Présentation intermédiaire (suivi des présentations des autres projets, présentation de BombusCar - Emerald Cottet, Francesco Carrino, Elena Mugelini, Omar Abou Khaled)
 - 15.10: 60 / Présentation intermédiaire (préparation pour la présentation orale)
 - 14.10: 60 / Présentation intermédiaire (travail sur le powerpoint)

Semaine A4
----------
 - 10.10: 60 / Correction et finalisation du cahier des charges et du planning
 - 09.10: 30 / Discussion avec Francesco Carrino sur l'analyse
 - 09.10: 35 / Séance de groupe (discussion sur le cahier des charges+planning+présentation - Francesco Carrino, Emerald Cottet, Omar Abou Khaled via skype)
 - 08.10: 120 / Correction et redaction du planning avec teamgantt (version 2)
 - 07.10: 30 / Correction et redaction du cahier des charges (version 2)

Semaine A3
----------
 - 02.10: 30 / Rédaction pv du dernier entretien
 - 02.10: 35 / Séance de groupe (discussion sur le cahier des charges+planning - Francesco Carrino, Emerald Cottet, Omar Abou Khaled via skype)
 - 02.10: 60 / Fin rédaction cahier des charges + planning + relecture (version 1)
 - 01.10: 60 / Rédaction cahier des charges + planning (version 1)
 - 30.09: 40 / Préparation cahier des charges + planning (formats, début rédaction) 

Semaine A2
----------
 - 26.09: 80 / Rédaction pvs des 2 entretiens
 - 25.09: 50 / Seconde séance (discussion approfondie sur le concept du projet - Francesco Carrino, Emerald Cottet)
 - 25.09: 30 / Première séance avec les superviseurs du projet (lancement officiel du projet - Omar Abou Khaled, Francesco Carrino, Emerald Cottet)
 - 25.09: 70 / Préparation environnement de travail (gitlab, formats rapport+pv)

Semaine A1
----------
 - 18.09: 240 / Découverte des projets de semestre 5 + choix du projet BombusCar (projet en binôme - Emerald Cottet)

Format adopté d'un logbook (Journal de travail)
===============================================

Contenu
-------
 * Le journal de travail est mis à jour de manière régulière (normalement chaque semaine)
 * Les semaines sont numérotées selon le calendrier académique de l'école (A1, A2,...)
 * Ordre chronologique inverse (le plus récent au début)
 * Toutes les durées sont en minutes
 * Un journal par étudiant sur le projet
 * Format et syntaxe :
   * DateJJ.MM: Durée / Tâche (résumé, remarques, impression,... - participant1, participant2,...)
   * Ce qui est entre parenthèse et optionnel

Style .md (markdown)
--------------------
 * Titre principal : ajouter une ligne de '=' sous le texte
 * Sous-titre (Semaine AX) : ajouter une ligne de '-' sous le texte, attention à laisser une ligne vide avant le texte
 * Liste à puce : un espace avant et après le tiret/l'astérisque pour le premier niveau (' - ' ou ' * ')
 * Liste imbriquée : deux espaces supplémentaires avant le tiret/l'astérisque pour un niveau supplémentaire

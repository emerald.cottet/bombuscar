BombusCar
=========

Contexte
--------

Projet de semestre 5 (2019-2020)

Titre du projet : BombusCar

Sujet complet : "BombusCar : Application mobile de covoiturage pour le transport de marchandises"

Proposé par : HumanTech Institute

Filière : Informatique (3ème année Bachelor) 

Informations générales 
----------------------

**Etudiants sur le projet :**
 - Cottet Emerald
 - Seydoux Quentin

**Responsables affiliés :**
 - Carrino Francesco
 - Mugellini Elena
 - Omar Abou Khaled

Description
-----------

Le but de ce projet et de réaliser la création d'un réseau de distribution intelligent, flexible et sécurisé pour le transport de colis en exploitant des capacités de transport déjà existantes.
Plus précisément, il est question de la création automatique du graph du réseau à partir des utilisateurs inscrits dans la plateforme et par la suite effectuer des tests avec des algorithmes d'optimisation.

Contenu
-------

Ce dépôt contient toute la documentation relative au projet dans le dossier `docs/`. Le code du projet est dans le dossier `code/`.

